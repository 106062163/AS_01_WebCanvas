# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
* ## **First** there is some js and css files i have included for this homework
    <h5>1. Bootstrap</h5>
    <h5>2. jquery</h5>
    <h5>3. hw1.css and hw1.js for homework appearance and features</h5>
    <img src="./readmeJpg/import.PNG"></img>
    <img src="./readmeJpg/import2.PNG"></img>
* ## And then i made a gif process once what i have done
    <img src="./readmeJpg/RunOneTime.gif" width="900px" height="500px"></img>
* ## Now i am going to introduce the interface
    * **<h3>interface:</h3>**
            <img src="./readmeJpg/interface.PNG"></img>
            <h4>1. Color Picker : using input color to implement</h4>
            <img src="./readmeJpg/pickColor.PNG"></img>
            <h4>2. Attributes : For changing <strong>line width, line style, graphics is fill or not, font size, and font family</strong></h4>
            <img src="./readmeJpg/Line&FontAttribute.PNG"></img>
                <h4>graphics that after changed their attributes</h4>
                <img src="./readmeJpg/showFeatures.PNG" width="400px" height="300px"></img>
            <h4>3. Rainbow Pen <img src="./readmeJpg/colorPen.PNG"></img></h4>
            <h4>After pressing pencil change to pencil brush mode, press pencil again change to rainbow pen</h4>
            <img src="./readmeJpg/colorPenDraw.PNG" width="400px" height="350px"></img>
            <h4>4. 新增: 上傳影片並播放在canvas上的功能,左鍵一下暫停/重新開始播放, 內容如以下gif顯示:</h4>
            <img src="./readmeJpg/upVideo.gif" width="400px" height="350px"></img>
            <h4>5. other button functions</h4>
        
        | **Button**                          | **Features** |
        | :--------------------------------------: | :----------: |
        | <img src="./image/pencil.png"></img>     | brush, can draw whatever you want, press button -> keep left click on canvas -> moveing mouse to draw                          |
        | <img src="./image/eraser.png"></img>     | eraser, can clear anything on the canvas, press button -> keep left click on canvas -> moveing mouse to clean        |
        | <img src="./image/slash.png"></img>      | line, can draw a straight line, press button -> keep left click on canvas -> moving mouse regulate the line-> release left click      |
        | <img src="./image/text_add.png"></img>   | text, input something and print on the canvas, press button-> left click on canvas-> type something -> press "enter" key of keyboard     |
        | <img src="./image/circle.png"></img>     | circle, draw a circle, press button-> keep left click on canvas sizing the circle-> release left click      |
        | <img src="./image/triangle.png"></img>   | triangle, draw a triangle, press button-> keep left click on canvas sizing the triangle-> release left click       |
        | <img src="./image/rectangular.png"></img>| rectangular, draw a rectangular, press button-> keep left click on canvas sizing the rectangular-> release left click      |
        | <img src="./image/reset.png"></img>      | reset, clear the canvas and undo/redo memory, press the button and click "確定" on alert window     |
        | <img src="./image/undo.png"></img>       | undo, press this button return to the previous step         |
        | <img src="./image/redo.png"></img>       | redo, press this button back to next step                                        |
        | <img src="./image/download.png"></img>   | download, press this button download the canvas picture as a png file(canvas.png)|
        | <img src="./image/upload.png"></img>     | upload, press this button upload a image and paste on the canvas     |
        

# End! That's all, thank you!