var canvas = document.getElementById("draw-canvas")
const ctx = canvas.getContext('2d');

var allBut = document.querySelectorAll("button")
var clearBut = document.getElementsByName("clearBut")
var rangeBar = document.getElementById("range-bar");
var pencilButton = document.getElementById("pencil")
var eraserButton = document.getElementById("eraser")
var lineButton = document.getElementById("line")
var circleButton = document.getElementById("circle")
var trianButton = document.getElementById("triangle")
var rectanButton = document.getElementById("rectangle")

var textButton = document.getElementById("textAdd")
var resetButton = document.getElementById("reset")
var downButton = document.getElementById("download")
var upButton = document.getElementById("upload")
var textInput = document.getElementById("input-text")
var textSize = document.getElementById("text-size")
var textFamily = document.getElementById("text-family")
var pickColor = document.getElementById("picked-color")
var uploadImg = document.getElementById("uploaded-img")
var drawWidth = document.getElementById("draw-width")
var joinLine = document.getElementById("line-join")
var canvasStep = []
var step = -1;
var lx, ly
var linestyle = "round"
var storeImg = new Image()
var fillDraw = true
var colorPen = false
var showVideo = false;
var videoPause = false;

var mouse = {
    state: "non",
    x: 0,
    y: 0,
    trigger: 0
}



var mouseDowned = false;
var MouseAction = () => {
    initStyle()
    if (mouseDowned) {
        switch (mouse.state) {
            case "pencil":
                pencilDraw();
                break
            case "eraser":
                ctx.strokeStyle = "#FFFFFF"
                pencilDraw();
                break;
            case "line":
                canvas.height = canvas.height
                ctx.drawImage(storeImg, 0, 0)
                ctx.moveTo(lx, ly)
                drawline();
                break;
            case "rect":
                canvas.height = canvas.height
                ctx.drawImage(storeImg, 0, 0)
                ctx.moveTo(lx, ly)
                drawrect();
                break
            case "circle":
                canvas.height = canvas.height
                ctx.drawImage(storeImg, 0, 0)
                drawcircle()
                break
            case "trian":
                canvas.height = canvas.height
                ctx.drawImage(storeImg, 0, 0)
                drawtrian()
                break
            case "text":
                textInput.focus()
                break;
        }
    }
    else {
        ctx.moveTo(mouse.x, mouse.y);
    }
    //console.log(mouse.x,mouse.y)
}

var colorNum1 = 255
var colorNum2 = 0
var colorNum3 = 0
var colorState = 0
var rgbToHex = function (rgb) {
    var hex = Number(rgb).toString(16);
    if (hex.length < 2) {
        hex = "0" + hex;
    }
    return hex;
};
var initStyle = () => {
    ctx.lineWidth = rangeBar.value
    ctx.lineJoin = linestyle
    if (colorPen && mouse.state == "pencil") {
        ctx.strokeStyle = "#" + rgbToHex(colorNum1) + rgbToHex(colorNum2) + rgbToHex(colorNum3)
        ctx.fillStyle = "#" + rgbToHex(colorNum1) + rgbToHex(colorNum2) + rgbToHex(colorNum3)
    }
    else {
        ctx.strokeStyle = pickColor.value
        ctx.fillStyle = pickColor.value
    }



}
var drawline = () => {
    initStyle()
    ctx.lineTo(mouse.x, mouse.y)
    ctx.closePath();
    ctx.stroke()
}
var drawcircle = () => {
    initStyle()
    ctx.arc(lx, ly, Math.sqrt(Math.pow(mouse.x - lx, 2) + Math.pow(mouse.y - ly, 2)), 0, Math.PI * 2, true);
    ctx.moveTo(mouse.x, mouse.y)
    if (fillDraw) ctx.fill()
    else ctx.stroke()

}
var drawrect = () => {
    initStyle()
    if (fillDraw) ctx.fillRect(lx, ly, mouse.x - lx, mouse.y - ly)
    else ctx.strokeRect(lx, ly, mouse.x - lx, mouse.y - ly)
}
var drawtrian = () => {
    initStyle()
    ctx.moveTo(lx + ((mouse.x - lx) / 2), ly)
    ctx.lineTo(mouse.x, mouse.y)
    ctx.lineTo(mouse.x - (mouse.x - lx), mouse.y)
    ctx.closePath()
    if (fillDraw) ctx.fill()
    else ctx.stroke()
}

var pencilDraw = () => {


    ctx.lineTo(mouse.x, mouse.y);
    ctx.closePath()
    ctx.stroke()

    ctx.beginPath()
    ctx.moveTo(mouse.x, mouse.y)

}

var updateMouseCor = (e) => {
    mouse.x = e.pageX - canvas.offsetLeft;
    mouse.y = e.pageY - canvas.offsetTop;
}

var singleClickEvent = () => {
    switch (mouse.state) {
        case "text":
            textInput.style.left = mouse.x + canvas.offsetLeft + "px";
            textInput.style.top = mouse.y + canvas.offsetTop - $("#text-size").val() / 2 + "px";
            textInput.style.display = "";
            textInput.style.fontSize = $("#text-size").val() + "px";
            textInput.style.fontFamily = $("#text-family").val()
            textInput.focus();
            break;
    }
}

function downImg() {
    var link = document.createElement('a');
    link.download = 'canvas.png';
    link.href = canvas.toDataURL('image/png');
    link.click();
}
function upImg() {
    $("#upImgField").click();
}
function storeCanvas() {
    step++;
    canvasStep.length = step + 1;
    canvasStep[step] = canvas.toDataURL()
}
window.addEventListener('load', function () {
    $("#draw-width").html(rangeBar.value)
    canvas.addEventListener("mousemove", updateMouseCor, false);
    canvas.addEventListener("mousedown", function () {

        if (showVideo == true) {
            if (videoPause == false) {
                videoPause = true
                v.pause()
            }
            else {
                videoPause = false;
                v.play()
            }
        }

        mouseDowned = true;

        mouse.trigger++;
        storeImg.src = canvas.toDataURL()
        lx = mouse.x
        ly = mouse.y
        singleClickEvent();
        if (mouse.state == "pencil") {
            initStyle()
            ctx.beginPath()
            ctx.moveTo(mouse.x, mouse.y)
            ctx.arc(mouse.x, mouse.y, rangeBar.value / 2, 0, Math.PI * 2, true)
            ctx.fill()
            ctx.closePath()
        }
        ctx.beginPath()
    }, false);
    canvas.addEventListener("mousemove", MouseAction, false);
    canvas.addEventListener("mouseup", function () {
        if (mouse.state != "non" && mouse.state != "text") storeCanvas()
        mouseDowned = false;
        //ctx.closePath()
    }, false)
    canvas.addEventListener("mouseout", () => {
        if (mouseDowned == true) {
            if (mouse.state != "text")
                storeCanvas()
            mouseDowned = false;
        }

    })
    pencilButton.onclick = () => {
        if (mouse.state != "pencil" || colorPen == true) {
            pencilButton.style.background = "url(image/pencil.png) no-repeat center white"
            colorPen = false;
        }
        else {
            pencilButton.style.background = "url(image/colorPen.png) no-repeat center white"
            colorPen = true;
        }
        mouse.state = "pencil";

        document.body.style.cursor = "url('cursor/pencil.cur'),auto";
    }
    eraserButton.onclick = () => { mouse.state = "eraser"; document.body.style.cursor = "url('cursor/eraser.cur'),auto"; }

    circleButton.onclick = () => { mouse.state = "circle"; document.body.style.cursor = "url('cursor/cross.cur'),auto"; }
    trianButton.onclick = () => { mouse.state = "trian"; document.body.style.cursor = "url('cursor/cross.cur'),auto"; }
    rectanButton.onclick = () => { mouse.state = "rect"; document.body.style.cursor = "url('cursor/cross.cur'),auto"; }

    lineButton.onclick = () => { mouse.state = "line"; document.body.style.cursor = "url('cursor/cross.cur'),auto"; }

    textButton.onclick = () => { mouse.state = "text"; document.body.style.cursor = "text"; }
    upButton.addEventListener('click', upImg)
    downButton.addEventListener('click', downImg)
    textInput.addEventListener("keyup", (e) => {
        if (e.keyCode === 13) {
            var newSize = $("#text-size").val() + "px";
            var newFamily = $("#text-family").val();
            ctx.font = newSize + ' ' + newFamily;
            ctx.fillStyle = pickColor.value;
            ctx.fillText(textInput.value, textInput.offsetLeft - canvas.offsetLeft,
                textInput.offsetTop - canvas.offsetTop + textInput.offsetHeight / 1.6, 1000)
            textInput.style.display = "none"
            textInput.style.top = 0;
            textButton.style.left = 0;
            textInput.value = ""
            storeCanvas()
            e.preventDefault();
        }
    })
    $("#reset").click(() => {
        if (confirm("Are you sure to reset all?")) {
            canvas.height = canvas.height;
            mouse.trigger = 0;
            mouse.state = "non";
            textInput.style.display = "none";
            document.body.style.cursor = "default";
            canvasStep = [];
            step = -1;
            colorPen = false;
            showVideo = false;
            videoPause = false;
            v.pause()
            v.ended()
            $("button").blur()
        }
    })
    $("#text-size").change(() => {
        textInput.style.fontSize = $("#text-size").val() + "px"
    })
    $("#text-family").change(() => {
        textInput.style.fontFamily = $("#text-family").val()
    })
    $("#picked-color").change(() => {
        textInput.style.color = $("#picked-color").val()
    })
    $("#undo").click(
        function () {
            if (step > 0) {
                step--;
                let canImg = new Image()
                canImg.src = canvasStep[step]
                canImg.onload = function () {
                    canvas.height = canvas.height;
                    ctx.drawImage(canImg, 0, 0);
                }
            }
            else {
                step = -1;
                canvas.height = canvas.height
            }
        }
    )
    $("#redo").click(
        function () {
            if (step < canvasStep.length - 1) {
                step++;
                let canImg = new Image()
                canImg.src = canvasStep[step]
                canImg.onload = function () {
                    canvas.height = canvas.height
                    ctx.drawImage(canImg, 0, 0)
                }
            }
        }
    )
    $("#upImgField").on('input', function (e) {
        let filetype = this.files[0].type
        if (filetype.indexOf('image') > -1) {
            let canImg = new Image()
            let inImg = e.target.files
            if (FileReader && inImg && inImg.length) {
                let fileR = new FileReader()
                fileR.readAsDataURL(inImg[0])
                fileR.onload = () => {
                    canImg.src = fileR.result
                }
            }
            canImg.onload = () => {
                ctx.drawImage(canImg, 0, 0);
                storeCanvas();
            }
        }
        else if (filetype.indexOf('video') > -1) {
            var videoP = document.getElementById("videoPlayer")
            videoP.src = URL.createObjectURL(this.files[0])
            showVideo = true
            videoP.play()
        }

    })
    $("#fill-draw").change(() => { fillDraw = ($("#fill-draw").val() == "Yes") ? true : false })
    $("button").click(() => {
        if (this.id != "textAdd") textInput.style.display = "none"
        if (mouse.state != "pencil") {
            pencilButton.style.background = "url(image/pencil.png) no-repeat center white"
            colorPen = false;
        }
    })
    rangeBar.addEventListener('mousedown', function () { drawWidth.innerHTML = this.value })
    rangeBar.addEventListener('mousemove', function () { drawWidth.innerHTML = this.value })
    joinLine.addEventListener('change', () => { linestyle = $("#line-join").val() })

    var v = document.getElementById("videoPlayer")
    var vi
    v.addEventListener('play', function () {
            vi = window.setInterval(function () {
                if(showVideo==true)
                ctx.drawImage(v, 0, 0, 700, 500)
            }, 20);
    }, false);
    v.addEventListener('pause', function () { window.clearInterval(vi); }, false);
    v.addEventListener('ended', function () { clearInterval(vi); }, false);


    setInterval(() => {

        if (mouseDowned && mouse.state == "pencil" && colorPen) {
            if (colorState == 0) colorNum2++
            else if (colorState == 1) colorNum1--
            else if (colorState == 2) colorNum3++
            else if (colorState == 3) colorNum2--
            else if (colorState == 4) colorNum1++
            else if (colorState == 5) colorNum3--
            switch (colorNum1.toString() + colorNum2.toString() + colorNum3.toString()) {
                case "25500":
                    colorState = 0
                    break
                case "2552550":
                    colorState = 1
                    break
                case "02550":
                    colorState = 2
                    break
                case "0255255":
                    colorState = 3
                    break
                case "00255":
                    colorState = 4
                    break
                case "2550255":
                    colorState = 5
                    break
            }
        }
        else {
            colorState = 0
            colorNum1 = 255
            colorNum2 = 0
            colorNum3 = 0
        }



    }, 2);

}, false)
